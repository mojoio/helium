# @mojoio/helium
a helium api package

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@mojoio/helium)
* [gitlab.com (source)](https://gitlab.com/mojoio/helium)
* [github.com (source mirror)](https://github.com/mojoio/helium)
* [docs (typedoc)](https://mojoio.gitlab.io/helium/)

## Status for master

Status Category | Status Badge
-- | --
GitLab Pipelines | [![pipeline status](https://gitlab.com/mojoio/helium/badges/master/pipeline.svg)](https://lossless.cloud)
GitLab Pipline Test Coverage | [![coverage report](https://gitlab.com/mojoio/helium/badges/master/coverage.svg)](https://lossless.cloud)
npm | [![npm downloads per month](https://badgen.net/npm/dy/@mojoio/helium)](https://lossless.cloud)
Snyk | [![Known Vulnerabilities](https://badgen.net/snyk/mojoio/helium)](https://lossless.cloud)
TypeScript Support | [![TypeScript](https://badgen.net/badge/TypeScript/>=%203.x/blue?icon=typescript)](https://lossless.cloud)
node Support | [![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
Code Style | [![Code Style](https://badgen.net/badge/style/prettier/purple)](https://lossless.cloud)
PackagePhobia (total standalone install weight) | [![PackagePhobia](https://badgen.net/packagephobia/install/@mojoio/helium)](https://lossless.cloud)
PackagePhobia (package size on registry) | [![PackagePhobia](https://badgen.net/packagephobia/publish/@mojoio/helium)](https://lossless.cloud)
BundlePhobia (total size when bundled) | [![BundlePhobia](https://badgen.net/bundlephobia/minzip/@mojoio/helium)](https://lossless.cloud)
Platform support | [![Supports Windows 10](https://badgen.net/badge/supports%20Windows%2010/yes/green?icon=windows)](https://lossless.cloud) [![Supports Mac OS X](https://badgen.net/badge/supports%20Mac%20OS%20X/yes/green?icon=apple)](https://lossless.cloud)

## Usage

Use TypeScript for best in class intellisense

```typescript
import { expect, tap } from '@pushrocks/tapbundle';
import * as helium from '@mojoio/helium';

let testHelium: helium.Helium;

tap.test('should create a valid helium class', async () => {
  testHelium = new helium.Helium();
  expect(testHelium).toBeInstanceOf(helium.Helium);
});

tap.test('should create and update a wallet', async () => {
  const wallet = await testHelium.addWalletByAddress('13L7By1BmboTx1hRUuN3MtMjNmTFaWj5C6EbRZCK2sceaRCWZev');
  expect(wallet).toBeInstanceOf(helium.HeliumWallet);
  console.log(wallet.earningsHnt24h);
  console.log(wallet.earningsHnt7d);
  console.log(wallet.earningsHnt14d);
  console.log(wallet.earningsHnt30d);
  console.log(wallet.hotspots);

  expect(wallet.hotspots[0]).toBeInstanceOf(helium.HeliumHotspot);

  // you can call wallet.update() at any time to update all wallet information
  // also wallet.hotspots[0].update() will only update an individual hotspot;
})

tap.start();
```

## Contribution

We are always happy for code contributions. If you are not the code contributing type that is ok. Still, maintaining Open Source repositories takes considerable time and thought. If you like the quality of what we do and our modules are useful to you we would appreciate a little monthly contribution: You can [contribute one time](https://lossless.link/contribute-onetime) or [contribute monthly](https://lossless.link/contribute). :)

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)

[![repo-footer](https://lossless.gitlab.io/publicrelations/repofooter.svg)](https://maintainedby.lossless.com)
