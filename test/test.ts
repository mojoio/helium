import { expect, tap } from '@pushrocks/tapbundle';
import * as helium from '../ts/index';

let testHelium: helium.Helium;

tap.test('should create a valid helium class', async () => {
  testHelium = new helium.Helium();
  expect(testHelium).toBeInstanceOf(helium.Helium);
});

tap.test('should create and update a wallet', async () => {
  const wallet = await testHelium.addWalletByAddress('13L7By1BmboTx1hRUuN3MtMjNmTFaWj5C6EbRZCK2sceaRCWZev');
  expect(wallet).toBeInstanceOf(helium.HeliumWallet);
  console.log(wallet.earningsHnt24h);
  console.log(wallet.earningsHnt7d);
  console.log(wallet.earningsHnt14d);
  console.log(wallet.earningsHnt30d);
  console.log(wallet.hotspots);

  expect(wallet.hotspots[0]).toBeInstanceOf(helium.HeliumHotspot);

  // you can call wallet.update() at any time to update all wallet information
  // also wallet.hotspots[0].update() will only update an individual hotspot;
})

tap.start();
