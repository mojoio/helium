import * as plugins from './helium.plugins';

import { HeliumWallet } from './helium.classes.heliumwallet';

export class Helium {
  public baseApiUrl = 'https://helium-api.stakejoy.com';
  public wallets: HeliumWallet[] = [];

  public async addWalletByAddress(walletAddressArg: string) {
    const newWallet = new HeliumWallet(this, walletAddressArg);
    await newWallet.update();
    this.wallets.push(newWallet);
    return newWallet;
  }

  public async request<T = any>(urlArg: string): Promise<T> {
    const webrequest = new plugins.webrequest.WebRequest();
    const response = await webrequest.request(urlArg, {
      method: 'GET',
      headers: {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36'
      }
    })
    return response.json();
  }
}
