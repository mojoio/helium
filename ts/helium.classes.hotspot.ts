import * as plugins from './helium.plugins';
import * as interfaces from './interfaces';

import { HeliumWallet } from './helium.classes.heliumwallet';

/**
 * a helium hotspot
 */
export class HeliumHotspot {
  public static async getAllHotspotsForWallet(walletArg: HeliumWallet) {
    const hotspotsResponse: interfaces.IWalletHotspotResponse = await walletArg.heliumRef.request(
      `${walletArg.heliumRef.baseApiUrl}/v1/accounts/13L7By1BmboTx1hRUuN3MtMjNmTFaWj5C6EbRZCK2sceaRCWZev/hotspots`
    );

    const returnHotspots: HeliumHotspot[] = [];
    for (const hotspotArg of hotspotsResponse.data) {
      const newHotspot = new HeliumHotspot(walletArg, hotspotArg.address);
      await newHotspot.update();
      returnHotspots.push(newHotspot);
    }
    return returnHotspots;
  }

  // INSTANCE

  heliumWalletRef: HeliumWallet;
  hotspotAddress: string;
  public originalData: interfaces.IHotspotResponse['data'];
  constructor(walletArg: HeliumWallet, hotspotAddress: string) {
    this.heliumWalletRef = walletArg;
    this.hotspotAddress = hotspotAddress;
  }

  public async update() {
    const hotspotResponse: interfaces.IHotspotResponse = await this.heliumWalletRef.heliumRef.request(
      `${this.heliumWalletRef.heliumRef.baseApiUrl}/v1/hotspots/${this.hotspotAddress}`
    );
    this.originalData = hotspotResponse.data
  }
}
