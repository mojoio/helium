import * as plugins from './helium.plugins';
import * as interfaces from './interfaces';

import {  Helium } from './helium.classes.helium';
import { HeliumHotspot } from './helium.classes.hotspot';

export class HeliumWallet {
  heliumRef: Helium;
  lastUpdate: plugins.smarttime.ExtendedDate;

  originalData: interfaces.IWalletResponse['data'];

  public walletAddress: string;
  public earningsHnt24h: number;
  public earningsHnt7d: number;
  public earningsHnt14d: number;
  public earningsHnt30d: number;

  public hotspots: HeliumHotspot[] = [];

  constructor(heliumRefArg: Helium, walletAddressArg: string) {
    this.heliumRef = heliumRefArg;
    this.walletAddress = walletAddressArg;
  }

  public async update() {
    const currentDate = new plugins.smarttime.ExtendedDate();

    const walletResponse: interfaces.IWalletResponse = await this.heliumRef.request(`${this.heliumRef.baseApiUrl}/v1/accounts/${this.walletAddress}`)
    this.originalData = walletResponse.data;

    const getHntEarningsForHours = async (hoursArg: number) => {
      const earningsResponse: interfaces.IWalletEarningsResponse = await this.heliumRef.request(`${this.heliumRef.baseApiUrl}/v1/accounts/${this.walletAddress}/rewards/sum?min_time=-${hoursArg}%20hour&max_time=${currentDate.toISOString()}&bucket=hour`);
      const reducedData:number = earningsResponse.data.map(dataArg => dataArg.sum).reduce((previous = 0, currentArg) => {
        return previous + currentArg;
      })
      return reducedData / 100000000;
    }

    this.hotspots = await HeliumHotspot.getAllHotspotsForWallet(this)
    
    
    this.earningsHnt24h = await getHntEarningsForHours(24);
    this.earningsHnt7d = await getHntEarningsForHours(24 * 7);
    this.earningsHnt14d = await getHntEarningsForHours(24 * 14);
    this.earningsHnt30d = await getHntEarningsForHours(24 * 30);
  }
}
