export interface IWalletResponse {
  data: {
    validator_count: number;
    staked_balance: number;
    speculative_sec_nonce: number;
    speculative_nonce: number;
    sec_nonce: number;
    sec_balance: number;
    nonce: number;
    hotspot_count: number;
    dc_nonce: number;
    dc_balance: number;
    block: number;
    balance: number;
    address: string;
  };
}

export interface IWalletEarningsResponse {
  meta: { min_time: string; max_time: string; bucket: 'hour' };
  data: {
    total: number;
    timestamp: string;
    sum: number;
    stddev: number;
    min: number;
    median: number;
    max: number;
    avg: number;
  }[];
}

export interface IWalletHotspotResponse {
  data: {
    lng: number;
    lat: number;
    timestamp_added: string;
    status: {
      timestamp: string;
      online: 'online' | 'offline';
      listen_addrs: string[];
      height: number;
    };
    reward_scale: number;
    payer: string;
    owner: string;
    nonce: number;
    name: string;
    mode: 'full' | 'light';
    location_hex: string;
    location: string;
    last_poc_challenge: number;
    last_change_block: number;
    geocode: {
      short_street: string;
      short_state: string;
      short_country: string;
      short_city: string;
      long_street: string;
      long_state: string;
      long_country: string;
      long_city: string;
      city_id: string;
    };
    gain: number;
    elevation: number;
    block_added: number;
    block: number;
    address: string;
  }[];
}
