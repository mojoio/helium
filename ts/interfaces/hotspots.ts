import { IWalletHotspotResponse } from './wallet';

export interface IHotspotResponse {
  data: IWalletHotspotResponse['data'][0];
}